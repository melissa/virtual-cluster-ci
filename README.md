# LXD Virtual Cluster

This directory is aimed at creating a virtual cluster for a gitlab-runner in Melissa CI.


## Requirements

* [distrobuilder](https://github.com/lxc/distrobuilder) (Suggests to install gonlang-go which may not be updated on the package manager. Instead, install golang from [here](https://go.dev/doc/install).)
* LXD 3.0 or above. We can also install `lxd` using `sudo snap install lxd`.




## Setup

If you never ran LXD before, call `lxd init` as superuser. Throghout this section, the text assumes that you use the default settings. In particular,
* the default storage pool is called `default` and
* containers can communicate with each other.

LXD manages storage with so-called _pools_. Inside a pool are individual _volumes_ where a volume can be thought of as the LXD equivalent of a computer's hard disk drive. On a cluster, the individual nodes managed by a batch scheduler usually share access to the user's home directory. To achieve a similar effect in LXD, several containers are made to work with the same volume. Create a shared volume called `shared` in the `default` storage pool:
```sh
lxc storage volume create default shared
```

This should yield the following Error message:
```
$ lxc something
If this is your first time running LXD on this machine, you should also run: lxd init
To start your first container, try: lxc launch ubuntu:18.04
Error: Get http://unix.socket/1.0: dial unix /var/snap/lxd/common/lxd/unix.socket: connect: permission denied
```
Cause:
* The command is not executed by the superuser and the user is not a member of the group `lxd`.
Fix:
* Execute as superuser with `sudo`
* Make user member of the `lxd` group: `sudo adduser <username> lxd`

*All* OAR and Slurm containers will access the same storage. Keep this in mind when having both a Slurm and an OAR virtual cluster at the same time.

## Flushing iptables (optional)
You may not be able to access the host's network when you intialize lxd at the beginning.
```sh
lxc launch ubuntu:22.04 <container-name>
lxc exec <container-name> -- ping google.com
```

This may be caused due to having docker installed ([Refer to the situation regarding iptables](https://discuss.linuxcontainers.org/t/containers-do-not-have-outgoing-internet-access/10844/4#:~:text=For%20the%20record,the%20same%20machine.)).

You can then execute the script `flush_iptable_rules.sh` to flush iptables and restart the LXD daemon. This will allow you to access the host's network.

Containers lose the network after each boot. To make the iptables persistent, Execute the following and check if the service has active status.

```sh
sudo apt-get install iptables-persistent
systemctl status netfilter-persistent.service
```

_Note: Install `iptables-persistent` after running the script as it will prompt whether you want to save currently added rules or not._

## Building the images and installing the runner

The `build-and-install.sh` script automates the process of building the image, creating the virtual cluster, installing the runner, and adding preparing a virtual environment on the front node with the Melissa dependencies. Call the script with a prefix that will namespace your cluster:

```sh
build-and-install.sh <namespace> <true/false> <branchname>
```
_Note: Check the parameter details inside the script before running._

_Default username is `john`_

Once this is completed, you still need to manually enter the cluster to register the runner. You can do so with the following commands:

```sh
# enter the frontnode as super user
lxc exec namespaceslurm-0 -- /bin/bash

# register the gitlab runner calling
/usr/local/bin/gitlab-runner register

# start the runner
/usr/local/bin/gitlab-runner start
```

The `register` command will ask you to enter the URL of the GitLab instance, the registration token, and the executor. The URL is the URL of the GitLab instance, e.g. `https://gitlab.inria.fr`. The registration token can be found in the GitLab project under Settings > CI/CD > Runners. The executor is `custom`. 

After you've completed this, you still need to update the `config.toml` file in the `gitlab-runner` container. You can do so with the following commands:

```sh
# enter the gitlab-runner container as super user
lxc exec namespaceslurm-0 -- /bin/bash

# edit the config.toml file to include the following
vim /etc/gitlab-runner/config.toml
```

Ensure the config.toml file contains the following lines:

```toml
[[runners]]
  name = "lxd-runner"
  url = "https://gitlab.inria.fr/"
  id = 6086
  token = "QtHx4Jj7rPCmHHe_KdZD"
  token_obtained_at = 2023-06-12T09:46:01Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "custom"
  builds_dir = "/home/john"
  cache_dir = "/home/john/cache"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.custom]
    run_exec = "/shared/gitlab-runner/gitlab-runner-executor-lxd/run.sh"
```

You should be able to confirm that your runner named `lxd-runner` is ready to be used in the GitLab project under Settings > CI/CD > Runners.

## Using the runner in your CI

You can add a stage to your CI using the tag `lxd-runner` to indicate that the stage should be executed on your virtual cluster. From there, your script executes as if you have logged into the virtual cluster manually. The following example shows how the virtual cluster is used in the CI to run the `examples/heat-pde/heat-pde-dl` example:

```yaml
integration:
  tags:
      - lxd-runner
  stage: integration_test
  before_script:
    - source /etc/profile.d/modules.sh
    - source /shared/john/.cienv/bin/activate
    - module load mpi
    - mkdir build install
    - python3 -m pip install --target=install -e $PWD 
    - cd build
    - cmake -DCMAKE_INSTALL_PREFIX=../install ../
    - make 
    - make install
    - cd ..
    - source melissa_set_env.sh
  script:
    - cd examples/heat-pde/
    - cd executables && mkdir build && cd build
    - cmake -DCMAKE_PREFIX_PATH=$MELISSA_INSTALL_PREFIX ..
    - make
    - cd ../../
    - sed -i "s|/path/to/melissa/examples/heat-pde|$(pwd)|g" ./heat-pde-dl/config_slurm.json
    - cd heat-pde-dl
    - melissa-launcher --config config_slurm.json
```


## Updating the dependencies in your virtual cluster

You can use the `build-and-install.sh` to update the python dependencies in the active cluster *without* rebuilding it. Call:

```sh
build-and-install.sh <namespace> true develop
```

Where the first argument is the namespace of the cluster you want to update, the second argument is a boolean for requesting to update or not, and the third is the string name for the melissa branch to update the dependencies for.
