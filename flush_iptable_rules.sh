#!/bin/bash

# Loop through iptables and ip6tables if docker is installed
if command -v docker &> /dev/null; then
    
    # Save current iptables rules
    iptables-save > /etc/iptables/rules.v4
    ip6tables-save > /etc/iptables/rules.v6
    
    for ipt in iptables iptables-legacy ip6tables ip6tables-legacy; do
        $ipt --flush
        $ipt --flush -t nat
        $ipt --delete-chain
        $ipt --delete-chain -t nat
        $ipt -P FORWARD ACCEPT
        $ipt -P INPUT ACCEPT
        $ipt -P OUTPUT ACCEPT
    done
    
    # Reload the LXD daemon
    systemctl reload snap.lxd.daemon
    printf "iptable flush successful.\n"
    
    # netfilter-persistent save
    printf "iptables are made persistent.\n"
else
	printf "Docker not found. No updates will take place.\n"
fi
