#!/bin/bash

# Copyright 2020 Institut national de recherche en informatique et en automatique (Inria)
#
# The code in this file is based on code found on GitLab.com.
# URL: https://docs.gitlab.com/runner/executors/custom_examples/lxd.html 
# Date: 2020-10-02

/bin/bash <"${1}"

if [ $? -ne 0 ]; then
    exit $BUILD_FAILURE_EXIT_CODE
fi
