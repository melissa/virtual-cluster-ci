#!/bin/sh
set -eux
# Script designed to automate the build and installation of a gitlab runner in a slurm virtual
# cluster

# ARG1: the name of the virtual cluster
# ARG2: (optional) a boolean that defaults to false. If true, then the script will only update the pip dependencies
# ARG3: (optional) the branch of the melissa repo to clone. Defaults to develop


CID=${1}
# third argument is a boolean that defaults to false
UPDATE=${2:-false}
BRANCH=${3:-develop}
WORKDIR=$(pwd)
UNAME=john
# if the user does not want to update, then they want to build from scratch
if [ "$UPDATE" = false ] ; then
    echo "Building and installing gitlab-runner in virtual cluster ${CID}"
    temp_dir=$(mktemp -d)  # Create a temporary directory and store its path in a variable
    cd "$temp_dir" 

    # ask for sudo to run only the following line
    # we build rockylinux 8 to match as close as possible to current jean-zay redhat 8
    sudo distrobuilder build-lxd $WORKDIR/lxd/rockylinux.yaml \
    -o image.release=8 \
    -o image.architecture=x86_64 \
    -o image.variant=virtual-cluster \
    -o source.variant=boot

    lxc image import \
    --alias 'auto-cluster/rockylinux/8/amd64' \
    -- incus.tar.xz rootfs.squashfs

    cd $WORKDIR

    python3 launch-virtual-cluster.py slurm 12 'auto-cluster/rockylinux/8/amd64' -v --prefix $CID --install-gitlab-runner
else
    echo "Only updating gitlab-runner in virtual cluster ${CID} for branch ${BRANCH}}"
fi
    
# remove any existing melissa repo
rm -rf melissa

# clone into melissa repo on BRANCH
git clone -b $BRANCH https://gitlab.inria.fr/melissa/melissa.git

# copy requirements files to container
lxc file push melissa/requirements.txt ${CID}slurm-0/shared/${UNAME}/requirements.txt
lxc file push melissa/requirements_dev.txt ${CID}slurm-0/shared/${UNAME}/requirements_dev.txt
lxc file push melissa/requirements_deep_learning.txt ${CID}slurm-0/shared/${UNAME}/requirements_deep_learning.txt

if [ "$UPDATE" = false ] ; then
    # copy the gitlab-runner folder to container
    lxc file push -r gitlab-runner-executor-lxd/ ${CID}slurm-0/shared/gitlab-runner/
else
    # delete the existing venv
    lxc exec ${CID}slurm-0 -- /bin/bash -c "rm -rf /shared/${UNAME}/.cienv"
fi

# make virtual env called .cienv
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- python3 -m venv .cienv

# make sure pip is upgraded
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "source .cienv/bin/activate && python3 -m pip install --upgrade pip"

# install requirements
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "source .cienv/bin/activate && module load mpi && python3 -m pip install -r /shared/${UNAME}/requirements.txt"
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "source .cienv/bin/activate && python3 -m pip install -r /shared/${UNAME}/requirements_dev.txt"
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "source .cienv/bin/activate && python3 -m pip install -r /shared/${UNAME}/requirements_deep_learning.txt"

# remove existing install if it exists
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "rm -rf adios2-install"
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "rm -rf adios2-build"
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "rm -rf ADIOS2"
# install adios
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "git clone https://github.com/ornladios/ADIOS2.git"
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "mkdir adios2-build"
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "module load mpi && cd adios2-build && cmake -DCMAKE_INSTALL_PREFIX=../adios2-install ../ADIOS2"
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "cd adios2-build && make -j 4"
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "cd adios2-build && make install"

# add the adios python lib to the python path
lxc exec ${CID}slurm-0 -- sudo --login --user ${UNAME} -- bash -c "echo 'PYTHONPATH=/shared/${UNAME}/adios2-install/lib/python3/dist-packages/' >> ~/.bashrc"


if [ "$UPDATE" = false ] ; then
    echo "Please follow the remaining manual instructions from README.md to complete the gitlab-runner registration/configuration"
else
    echo "Finished updating dependencies for gitlab-runner in virtual cluster ${CID} for branch ${BRANCH}"
fi

# clean up the melissa sources
rm -rf melissa
